public class Test {
    public static void main(String[] args) {
        MyNode root = new MyNode("root");

        root.setLeft(
                new MyNode("left-1").setLeft(
                        new MyNode("left-2")
                )
        );

        root.setRight(
                new MyNode("right-1").setLeft(
                        new MyNode("left-3")
                ).setRight(
                        new MyNode("right-3")
                )
        );

        root.print("");

        System.out.println(root.contains("test"));
        System.out.println(root.contains("right-3"));
    }
}
