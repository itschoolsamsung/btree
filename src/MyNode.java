public class MyNode {
    private String value;
    private MyNode left;
    private MyNode right;

    MyNode(String value) {
        setValue(value);
    }

    public MyNode setValue(String value) {
        this.value = value;

        return this;
    }

    public MyNode setLeft(MyNode left) {
        this.left = left;

        return this;
    }

    public MyNode setRight(MyNode right) {
        this.right = right;

        return this;
    }

    public void print(String space) {
        System.out.println(space + value);

        if (left != null) {
            left.print(space + "---");
        }

        if (right != null) {
            right.print(space + "---");
        }
    }

    public boolean contains(String findValue) {
        if (findValue.equals(this.value)) {
            return true;
        }

        if (left != null && left.contains(findValue)) {
            return true;
        }

        if (right != null && right.contains(findValue)) {
            return true;
        }

        return false;
    }
}
